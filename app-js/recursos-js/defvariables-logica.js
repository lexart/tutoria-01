let hola 		= "Hola mundo";
const API 		= "http://localhost:8080";

console.log("variable hola ::", hola);
console.log("constante API ::", API);



let booleano = true // false
let num = 10.2 // O un entero 10
let txt = "Hola Mundo" // String
let obj = {
	nombre: "Alex",
	apellido: "Casadevall",
	metodo: function (){
		return "hola"
	}
} // OBJETO
let arr = [
	"Hola", "mundo"
]
// ARRAY

console.log("num: ", num)
console.log("txt: ", txt)
console.log("obj: ", obj)
console.log("arr: ", arr)


// LOGICA
// COMPARADOR LOGICO ALGEBRAICO: A y B, NUMEROS
// ENTONCES SE COMPARAN
// > mayor 
// < menor
// >= mayor igual
// <= menor igual

if(num > 10){ 
	console.log("num es mayor")
} else {
	console.log("num es menor")
}

// COMPARADORES LOGICOS
	// A == B 
	// A != B
	// A, B ?? 

let A = "10"
let B = 10

if(A == B){
	console.log("son iguales ::")
} else {
	console.log("son diferentes ::")
}

if(A === B){
	console.log("son equivalentes ::")
} else {
	console.log("son inequivalentes ::")
}
// LOGICOS
	// condicional logica: 
	// COND: 
		// cond(i) ~ [true, false]
let cond1 = 10 > 5;
let cond2 = !false;
let cond5 = false;

let cond3 = cond1 && cond2; // cond1 ~ ture, cond2 ~ true 
// cond1 && cond2: es true si todas las condicionales son TRUE
// si existe por lo menos 1 falsa la resultante es falsa

let cond4 = cond1 || cond5; // cond1 ~ true, cond5 ~ false
// cond1 OR cond5: es false si todas las condicionales son FALSE
// si existe por lo menos 1 true la resultante es true


