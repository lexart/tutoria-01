let tagTexto = function (tag) {
	// #id ~ es una ID
	// .clase ~ es una clase
	// si no tiene . ni # ~ es una etiqueta 

	// SUPONER QUE SIEMPRE SON ETIQUETAS
	let tagList 	= document.querySelectorAll(tag);
	let arrInner 	= [];
	let i; 
	let total = tagList.length - 1;

	for (i = total; i >= 0; i--) {
		// OBTENER LOS INNER TEXT
		// INGRESAR PARA EL ARRAY
		arrInner.push(tagList[i].innerText)
	}
	return arrInner;
}

let addResult = function (arrTextos) {
	let arrTxts = arrTextos;
	let txt 	= "";

	for (let i = arrTxts.length - 1; i >= 0; i--) {
		txt += arrTxts[i] + "\n";
	}

	document.getElementById('resultado').innerText = txt;
}

// EVENTO DESDE EL HTML ~ ONCLICK
let agregarTexto = function () {
	let arr = tagTexto('p');
	addResult(arr);
}
// BORRAR TEXTO
let clearTexto = function () {
	document.getElementById('resultado').innerText = "";
}


// LO MISMO QUE HACE JQUERY
let btn = document.getElementById('otroBtn');

btn.addEventListener('click', function () {
	agregarTexto()
})





