let arr = [
	"hola",
	"mundo",
	"no",
	"me",
	"lloren",
	"mas",
	10,
	20,
	150,
	100,
	12
]

let obj = {
	nombre: "Alex",
	apellido: "Casadevall",
	profesion: "Castigador",
	cedula: "123213"
}

// ITERAR OBJETOS
let i;
let total = arr.length;

// ARRAYS
for (i = total - 1; i >= 0; i--) {
	// i = 0; i < total; i++

	if(typeof arr[i] == "number"){
		console.log("encontre un numero: ", arr[i])
	} else {
		console.log("no encontre nada ...")
	}
}

// OBJETOS
for (key in obj){
	if(key == "nombre"){
		console.log("ob item: ", key + ":", obj[key])
	} else if(key == "apellido"){
		console.log("hide apellido")
	} else {
		console.log("nada ...")
	}
}

// CORTAR LA EJECUCION DE UN LOOP
for (i = total - 1; i >= 0; i--) {
	// i = 0; i < total; i++

	if(typeof arr[i] == "number" && arr[i] == 20){
		console.log("encontre 20: ", arr[i]);
		// CORTAR LOOP
		break;
	} else if (typeof arr[i] == "number"){
		console.log("numero: ", arr[i])
		// break;
	} else {
		console.log("al pedo ...")
	}
}



