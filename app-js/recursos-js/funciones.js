// METODOS (FUNCIONES)

let nombreApellido = function (){
	return "Alex" + " Casadevall"
}

let fullName = nombreApellido();

let nombreApellidoLog = function (){
	console.log("Alex Casadevall");
}


// 
let persona = {
	nombre: "Alex",
	apellido1: "Casadevall",
	apellido2: "Correa",
	edad: 28,
	cedula: "123213"
}

let funFullName = function (nombre, apellido) {
	let space = " ";
	return nombre + space + apellido;
}

let funFullName1 = function (nombre, apellido1, apellido2) {
	let space = " ";
	return nombre + space + apellido1 + space + apellido2;
}

let funFullNameObj = function (persona){
	// ESTO ES UN OBJETO Y NO UN ARRAY
	if(typeof persona == "object" && !persona.hasOwnProperty('length')){
		let space = " ";
		return persona.nombre + space + persona.apellido1 + space + persona.apellido2;
	} else {
		return "Juan Nadie";
	}
}





