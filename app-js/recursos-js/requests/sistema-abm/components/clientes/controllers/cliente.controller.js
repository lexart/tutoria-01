let clienteController = function (ServicioCliente) {
	
	let viewListadoCliente = function(){
		// GET - REQ #1
		ServicioCliente.views('./components/clientes/views/indexView.html', function (view) {
			appCont.innerHTML = view;

			// YA SE QUE LA TABLA ESTA DISPONIBLE
			let tbody = document.querySelectorAll('#listadoClientes tbody');
			let tr    = ``;

			// GET - REQ #2
			ServicioCliente.obtenerTodos('/clientes', function (clientes){

				for (let i = clientes.length - 1; i >= 0; i--) {
					let item = clientes[i];

					tr += `
						<tr>
							<td>${item._id}</td>
							<td>${item.nombre}</td>
							<td>${item.apellido}</td>
							<td>${item.email}</td>
							<td>
								<button class="btn btn-primary" onclick="clienteController(ServicioCliente).verCliente('${item._id}')">
									Editar
								</button>
								<button class="btn btn-danger" onclick="clienteController(ServicioCliente).eliminarCliente('${item._id}')">
									Eliminar
								</button>
							</td>
						</tr>
					`
				}
				tbody[0].innerHTML = tr;
				
				console.log("tbody: ", tbody);
			})

		})
	}

	let viewFormCliente = function () {
		// GET - REQ #1
		ServicioCliente.views('./components/clientes/views/formView.html', function (view) {
			appCont.innerHTML = view;
		})
	}

	let verCliente 	    = function (id){
		// REQ #1
		ServicioCliente.views('./components/clientes/views/formView.html', function (view) {
			appCont.innerHTML = view;

			// VOY A CARGAR LOS DATOS EN EL FORMULARIO
			let inputs = document.querySelectorAll('form input');

			// REQ #2
			ServicioCliente.obtenerUno('/clientes/' + id, function (cliente){
				inputs.forEach( function (item){
					// CARGA LA DATA DE LOS VALUES
					// EN LOS INPUT CORRESPONDIENTE
					item.value = cliente[item.name]; 
					// it 1: cliente["nombre"]
					// it 2: cliente["apellido"]
					// it 3: cliente["email"]
				})
			})
		})
	}

	let guardarCliente = function (){
		let inputs 	= document.querySelectorAll('form input');
		let cliente = {};

		inputs.forEach( function (input){
			cliente[input.name] = input.value;
		})

		console.log("form cliente: ", cliente)

		// VERIFICAR SI VIENE _id
		if(!cliente._id || cliente._id == ""){
			delete cliente._id;
			ServicioCliente.crearItem(cliente, '/clientes', function (res){
				console.log("respuesta crudcrud: ", res);
				alert("Cliente creado correctamente!")
			})
		} else {
			ServicioCliente.actualizarItem(cliente, '/clientes/' + cliente._id, function (res){
				console.log("respuesta crudcrud: ", res);
				alert("Cliente actualizado correctamente!")
			})
		}
	}

	let eliminarCliente = function (id){
		if(confirm("Estas seguro que quiere eliminar este cliente?")){
			// DELETE - REQ #1
			ServicioCliente.eliminarItem('/clientes/' + id, function (res){
				console.log("respuesta crudcrud: ", res);
				viewListadoCliente();
			})
		}
	}

	// $rootScope
	// window.viewFormCliente = viewFormCliente;

	// RETORNAR FUNCIONES PARA LA VISTA
	return {
		viewFormCliente: viewFormCliente,
		viewListadoCliente: viewListadoCliente,
		guardarCliente: guardarCliente,
		verCliente: verCliente,
		eliminarCliente: eliminarCliente
	}
}