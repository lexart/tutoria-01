let productoController = function (ServicioProducto) {
	
	let viewListadoProducto = function(){
		// GET - REQ #1
		ServicioProducto.views('./components/productos/views/indexView.html', function (view) {
			appCont.innerHTML = view;

			// YA SE QUE LA TABLA ESTA DISPONIBLE
			let tbody = document.querySelectorAll('#listadoProductos tbody');
			let tr    = ``;

			// GET - REQ #2
			ServicioProducto.obtenerTodos('/productos', function (productos){

				for (let i = productos.length - 1; i >= 0; i--) {
					let item = productos[i];

					tr += `
						<tr>
							<td>${item._id}</td>
							<td>${item.nombre}</td>
							<td>${item.precio}</td>
							<td>${item.categoria}</td>
							<td>
								<button class="btn btn-primary" onclick="productoController(ServicioProducto).verProducto('${item._id}')">
									Editar
								</button>
								<button class="btn btn-danger" onclick="productoController(ServicioProducto).eliminarProducto('${item._id}')">
									Eliminar
								</button>
							</td>
						</tr>
					`
				}
				tbody[0].innerHTML = tr;
				
				console.log("tbody: ", tbody);
			})

		})
	}

	let viewFormProducto = function () {
		// GET - REQ #1
		ServicioProducto.views('./components/productos/views/formView.html', function (view) {
			appCont.innerHTML = view;
		})
	}

	let verProducto 	    = function (id){
		// REQ #1
		ServicioProducto.views('./components/productos/views/formView.html', function (view) {
			appCont.innerHTML = view;

			// VOY A CARGAR LOS DATOS EN EL FORMULARIO
			let inputs = document.querySelectorAll('form input');

			// REQ #2
			ServicioProducto.obtenerUno('/productos/' + id, function (Producto){
				inputs.forEach( function (item){
					// CARGA LA DATA DE LOS VALUES
					// EN LOS INPUT CORRESPONDIENTE
					item.value = Producto[item.name]; 
					// it 1: Producto["nombre"]
					// it 2: Producto["apellido"]
					// it 3: Producto["email"]
				})
			})
		})
	}

	let guardarProducto = function (){
		let inputs 	= document.querySelectorAll('form input');
		let Producto = {};

		inputs.forEach( function (input){
			Producto[input.name] = input.value;
		})

		console.log("form Producto: ", Producto)

		// VERIFICAR SI VIENE _id
		if(!Producto._id || Producto._id == ""){
			delete Producto._id;
			ServicioProducto.crearItem(Producto, '/productos', function (res){
				console.log("respuesta crudcrud: ", res);
				alert("Producto creado correctamente!")
			})
		} else {
			ServicioProducto.actualizarItem(Producto, '/productos/' + Producto._id, function (res){
				console.log("respuesta crudcrud: ", res);
				alert("Producto actualizado correctamente!")
			})
		}
	}

	let eliminarProducto = function (id){
		if(confirm("Estas seguro que quiere eliminar este Producto?")){
			// DELETE - REQ #1
			ServicioProducto.eliminarItem('/productos/' + id, function (res){
				console.log("respuesta crudcrud: ", res);
				viewListadoProducto();
			})
		}
	}

	// $rootScope
	// window.viewFormProducto = viewFormProducto;

	// RETORNAR FUNCIONES PARA LA VISTA
	return {
		viewFormProducto: viewFormProducto,
		viewListadoProducto: viewListadoProducto,
		guardarProducto: guardarProducto,
		verProducto: verProducto,
		eliminarProducto: eliminarProducto
	}
}