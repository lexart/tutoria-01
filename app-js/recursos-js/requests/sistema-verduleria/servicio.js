let API   	  = "https://crudcrud.com/api/ee118d1caf594d12aa88c5c6601f18a6";
let xhttp 	  = new XMLHttpRequest();

let Servicio  = {
	obtenerTodos: function (model, callback){
		xhttp.onreadystatechange = function () {
			if (xhttp.readyState == 4) {
				productos = JSON.parse(xhttp.responseText);

				// DEVUELVE ARR DE PRODUCTOS
				callback(productos);
			}
		}

		// OBTENER LOS DATOS POR GET
		xhttp.open('GET', API + model);
		xhttp.send();
	},
	crearItem: function (objeto, model, callback){
		xhttp.onreadystatechange = function () {
				if (xhttp.readyState == 4) {
					let response = {
						"success": true
					}

					callback(response);
				}
			}

		xhttp.open('POST', API + model);
		xhttp.setRequestHeader("Content-type", "application/json");
		xhttp.send( JSON.stringify(objeto) );
	}
}