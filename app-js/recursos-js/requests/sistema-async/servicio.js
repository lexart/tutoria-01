let ServicioCliente  = {
	views: function (url, callback){
		let view = ``;
		xhttp.onreadystatechange = function () {
			if (xhttp.readyState == 4) {
				view = xhttp.responseText;

				// DEVUELVE ARR DE PRODUCTOS
				callback(view);
			}
		}

		// OBTENER LOS DATOS POR GET
		xhttp.open('GET', url);
		xhttp.send();
	},
	obtenerTodos: function (model, callback){
		xhttp.onreadystatechange = function () {
			if (xhttp.readyState == 4) {
				productos = JSON.parse(xhttp.responseText);

				// DEVUELVE ARR DE PRODUCTOS
				callback(productos);
			}
		}

		// OBTENER LOS DATOS POR GET
		xhttp.open('GET', API + model);
		xhttp.send();
	},
	obtenerTodosSync: function (model){
		return new Promise ( function (resolve, reject){
			xhttp.onreadystatechange = function () {
				if (xhttp.readyState == 4) {
					productos = JSON.parse(xhttp.responseText);

					// DEVUELVE ARR DE PRODUCTOS
					resolve(productos);
				}
			}

			// OBTENER LOS DATOS POR GET
			xhttp.open('GET', API + model);
			xhttp.send();
		})
	},
	obtenerUno: function (model, callback){
		xhttp.onreadystatechange = function () {
			if (xhttp.readyState == 4) {
				cliente = JSON.parse(xhttp.responseText);

				// DEVUELVE ARR DE PRODUCTOS
				callback(cliente);
			}
		}

		// OBTENER LOS DATOS POR GET
		xhttp.open('GET', API + model);
		xhttp.send();
	},
	obtenerUnoSync: function (model){
		return new Promise ( function (resolve, reject){
			xhttp.onreadystatechange = function () {
				if (xhttp.readyState == 4) {
					cliente = JSON.parse(xhttp.responseText);

					// DEVUELVE ARR DE PRODUCTOS
					resolve(cliente);
				}
			}

			// OBTENER LOS DATOS POR GET
			xhttp.open('GET', API + model);
			xhttp.send();
		})
	},
	crearItem: function (objeto, model, callback){
		xhttp.onreadystatechange = function () {
				if (xhttp.readyState == 4) {
					let response = {
						"success": true
					}

					callback(response);
				}
			}

		xhttp.open('POST', API + model);
		xhttp.setRequestHeader("Content-type", "application/json");
		xhttp.send( JSON.stringify(objeto) );
	},
	actualizarItem: function (objeto, model, callback){
		delete objeto._id; // ELIMINAR LA PROP _id 
		// POR SOPORTE DE CRUD CRUD

		xhttp.onreadystatechange = function () {
				if (xhttp.readyState == 4) {
					let response = {
						"success": true
					}

					callback(response);
				}
			}

		xhttp.open('PUT', API + model); // METODO PUT ES ACTUALIZAR
		xhttp.setRequestHeader("Content-type", "application/json");
		xhttp.send( JSON.stringify(objeto) );
	},
	eliminarItem: function (model, callback){
		
		xhttp.onreadystatechange = function () {
				if (xhttp.readyState == 4) {
					let response = {
						"success": true
					}

					callback(response);
				}
			}

		xhttp.open('DELETE', API + model); // METODO PUT ES ACTUALIZAR
		xhttp.send();
	}
}