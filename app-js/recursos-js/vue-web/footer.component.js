// COMO RESOLVER UN TEMPLATE ASYNC
// DEFINO UNA FUNCION
Vue.component('footer-item', function (resolve, reject){
	// GENERO LA REQUEST PARA TRAERME EL VIEW POR EL PATH
	Servicio.view('./shared/footerView.html', function (view){
		// SI TODO ES OK 
		// RESUELVE EL OBJ ANTERIOR
		resolve({
			props:['title','year'],
			template: view
		})
	})
})