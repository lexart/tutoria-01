// INSTANCIAMOS NUESTRA APP DE VUE
let app = new Vue({
	el: '#app',
	data: {
		titulo: 'Mi blog!',
		anio:'2020',
		menuItems: [
			{
				nombre: "Home",
				link: "./"
			},
			{
				nombre: "Blog",
				link: "./blog.html"
			},
			{
				nombre: "Ayuda",
				link: "#ayuda"
			},
			{
				nombre: "Contacto",
				link: "./contact.html"
			}
		]	
	}
})