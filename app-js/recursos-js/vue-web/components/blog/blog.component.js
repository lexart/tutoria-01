Vue.component('blog-component', function (resolve, reject){
	// GENERO LA REQUEST PARA TRAERME EL VIEW POR EL PATH
	Servicio.view('./components/blog/blogView.html', function (view){
		// SI TODO ES OK 
		// RESUELVE EL OBJ ANTERIOR
		resolve({
			props:['title'],
			template: view,
			data: function (){
					return {
						itemsBlog: [
							{
								titulo: "Hola mundo",
								parrafo: `
									lorem lorem lorem ...
								`,
								img: 'https://vuejs.org/images/logo.png',
								btnText: 'Leer más'
							},
							{
								titulo: "Hola mundo II",
								parrafo: `
									lorem lorem lorem ...
								`,
								img: 'https://vuejs.org/images/logo.png',
								btnText: 'Leer más'
							},
							{
								titulo: "Hola mundo III",
								parrafo: `
									lorem lorem lorem ...
								`,
								img: 'https://vuejs.org/images/logo.png',
								btnText: 'Leer más'
							}
						],
						text: ''
				}
				
			},
			methods: {
				addholaMundo: function (){
					this.text = 'Hola Mundo'
				}
			}
		})
	})
})