Vue.component('menu-component', function (resolve, reject){
	// GENERO LA REQUEST PARA TRAERME EL VIEW POR EL PATH
	Servicio.view('./shared/menuView.html', function (view){
		// SI TODO ES OK 
		// RESUELVE EL OBJ ANTERIOR
		resolve({
			props:['title','items'],
			template: view
		})
	})
})