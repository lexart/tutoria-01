let clienteCtrl = {
	render: function (){
		console.log("clienteCtrl ::")

		let tableClientes = document.getElementById('tablaClientes');
		let tbody 		  = tableClientes.tBodies[0];

		let tplTR 		  = ``;

		// 

		let arrClientes = [
			{
				nombre: "Alex",
				apellido: "Casadevall",
				id: 123
			},
			{
				nombre: "Juan",
				apellido: "Casadevall",
				id: 111
			},
			{
				nombre: "Gabriel",
				apellido: "Casadevall",
				id: 1111111
			},
			{
				nombre: "Diego",
				apellido: "Casadevall",
				id: 222
			}
		]
		
		for (let i = arrClientes.length - 1; i >= 0; i--) {
			let cliente = arrClientes[i];

			tplTR += `
				<tr>
					<td>${cliente.id}</td>
					<td>${cliente.nombre}</td>
					<td>${cliente.apellido}</td>
					<td class="text-right">
						<button class="btn btn-primary" onclick="app.clienteCtrl.editarCliente(${cliente.id})">
							Editar
						</button>
						<button class="btn btn-danger" onclick="eliminarCliente(${cliente.id})">
							Eliminar
						</button>
					</td>
				</tr>
			`
		}
		tableClientes.tBodies[0].innerHTML = tplTR;

		console.log("arrClientes: ", arrClientes);
	},
	editarCliente: function (id){
		console.log("editarCliente - id: ", id)
	}
}






