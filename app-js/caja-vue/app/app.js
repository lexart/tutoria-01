let app = new Vue({
	el: '#app',
	data: {
		mensaje: 'Hola Mundo!',
		productos: [
			{
				codigo:"ABC123",
				nombre:"Manzana",
				precio:100,
				cantidad: 1,
				moneda: "\$"
			},
			{
				codigo:"ABC111",
				nombre:"Banana",
				precio:65,
				cantidad: 1,
				moneda: "\$"
			},
			{
				codigo:"ABC222",
				nombre:"Pera",
				precio:120,
				cantidad: 1,
				moneda: "\$"
			}
		],
		resultados: [],
		cajaLista: [],
		busqueda: '',
		subTotal: 0,
		total: 0,
		impuestos: 1.22,
		cierreTicket: false,
		fecha: ''
	},
	methods: {
		buscarProductos: function () {
			console.log("buscar ::", this.busqueda)

			// QUIERO BUSCAR
			// POR CODIGO Y POR NOMBRE

			// BUSQUEDA PARCIAL: El término "ABC" Incluido en nombre o codigo
			// BUSQUEDA COMPLETA: "ABC123" == nombre o == codigo

			// NORMALIZAR TERMINOS
			// AbC == "ABC" ~ lowercase("AbC"), lowercase("ABC") ~ "abc" == "abc"

			// SELECT * FROM productos WHERE name LIKE '%item%' OR code LIKE '%item%'

			let resultado = [];
			this.productos.map( (item) => {
				// 
				// BUSQUEDA COMPLETA
				// if(item.nombre == this.busqueda || item.codigo == this.busqueda){
				// 	resultado.push( item );
				// }
				// 

				// BUSQUEDA TERMINOS
				let buscar = this.busqueda.toLowerCase();

				if(item.nombre.toLowerCase().includes(buscar) || item.codigo.toLowerCase().includes(buscar)){
					resultado.push( item );
				}
			})

			// function (item){ this ~ a esta función }
			// (item) => { this ~ hace referencia al padre}

			this.resultados = resultado;
		},
		agregarCaja: function (item){
			// HACER UNA COPIA DEL OBJETO ITEM
			let itemCarrito = JSON.parse(JSON.stringify(item))
			this.cajaLista.push(itemCarrito);

		 	// LLAMO PROC SUBTOTAL
		 	this.procSubTotal()
		},
		eliminarItemCaja: function (ind) {
			this.cajaLista.splice(ind, 1);

			this.procSubTotal();
		},
		procSubTotal: function () {
			let subtotal = 0;
			this.cajaLista.map( (item) => {
				subtotal += item.precio * item.cantidad;
			})

			this.subTotal = subtotal;
		},
		cierre: function (){
			this.cierreTicket = true;
			this.fecha = (new Date()).toISOString().split('T')[0];
		},
		volver: function (){
			this.cierreTicket = false;
		}
	},
	mounted: function (){

	}
})