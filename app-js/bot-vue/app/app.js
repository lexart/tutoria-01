let app = new Vue({
	el: '#app',
	data: {
		mensajes: [
			// {
			// 	type:'bot',
			// 	mensaje: 'Hola Mundo'
			// },
			// {
			// 	type:'me',
			// 	mensaje: 'Buenas tardes'
			// },
			// {
			// 	type:'bot',
			// 	mensaje: 'En que te puedo ayudar?'
			// }
		],
		mensaje: '',
		conversacion: [],
		estado: 0
	},
	methods: {
		// CONVERSACION
		convBot: function (msg, state){
			let botMsg 	= {type:'bot', mensaje:'No entendí.'};
			let success = false; 

			let arrConversacion = [
				{
					"terms": {
						'bot':'Hola, como estas?',
					},
					"estado":-1,
					"typeMsg":'text'
				},
				{
					"terms": {
						'hola':'En que te puedo ayudar?',
						'bien':'En que te puedo ayudar?',
						'mal':'En que te puedo ayudar?',
						'a':'En que te puedo ayudar?',
						'e':'En que te puedo ayudar?',
						'i':'En que te puedo ayudar?',
						'o':'En que te puedo ayudar?',
						'u':'En que te puedo ayudar?'
					},
					"estado":0,
					"typeMsg":'text'
				},
				{
					"terms": {
						'bot':'Necesito tu usuario',
					},
					"estado":1,
					"typeMsg":'text'
				},
				{
					"terms": {
						'user':'ahora necesito tu clave',
					},
					"estado":2,
					"typeMsg":'text'
				},
				{
					"terms": {
						'hola111':'tu clave es correcta',
					},
					"estado":3,
					"typeMsg":'text'
				},
				{
					"terms": {
						'bot':'En que te puedo ayudar?',
					},
					"estado":4,
					"typeMsg":'text'
				},
				{
					"terms": {
						'quiero':`
							1. "Quieres pedir un préstamo?" <br>
							2. "Condiciones para préstamos" <br>
							3. "Ayuda"
						`,
						'prestamo':`
							1. "Quieres pedir un préstamo?" <br>
							2. "Condiciones para préstamos" <br>
							3. "Ayuda"
						`,
					},
					"estado":5,
					"typeMsg":'html'
				},
				{
					"terms": {
						'1':`Opción 1: Lorem ipsum dolor sit amet, consectetur adipisicing.`,
						'2':`Opción 2: Lorem ipsum dolor sit amet, consectetur adipisicing.`,
						'3':`Ayuda: Lorem ipsum dolor sit amet, consectetur adipisicing.`
					},
					"estado":6,
					"typeMsg":'text'
				}
			]

			let obj = {};

			arrConversacion.map( (item) => {
				// ENCONTRO EL TERMINO POR EL ESTADO
				if(item.estado == state){
					// CARGA EL TERMINO DE X OBJETO
					obj = {terms: item.terms, typeMsg: item.typeMsg};
					return false;
				}
			})

			// ACA PROCESA EL DIALOGO
			for (key in obj.terms){
				// PREGUNTA SI EN EL MENSAJE DEL USUARIO
				// ESTA EL TERMINO DENTRO DE TERMS
				if(msg.includes(key)){
					botMsg 	= {type: 'bot', mensaje: obj.terms[key], typeMsg: obj.typeMsg};
					success = true;
					break;
				}
			}

			if(success){
				// CAMBIO DE ESTADO
				this.estado++;
			} else {
				this.estado = -1;
			}

			return botMsg;
		},
		addMensaje: function (){
			let msg = {
				type:'me',
				mensaje: this.mensaje
			}
			this.mensajes.push(msg);

			// RESPUESTA DEL BOT
			// this.mensaje ~ Mensaje del usuario
			// this.estado 	~ hilo de la conversación

			let botSay = this.convBot(this.mensaje, this.estado);

			// AGREGO A LOS MENSAJES
			this.mensajes.push(botSay);

			if(this.estado == 1 || this.estado == 4){
				let botSay = this.convBot('bot', this.estado);
				this.mensajes.push(botSay);
			}


			if(this.estado == -1){
				let botSay = this.convBot('bot', this.estado);
				this.mensajes.push(botSay);
			}

			this.mensaje = ''
		}
	}
})