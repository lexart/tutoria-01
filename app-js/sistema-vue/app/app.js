// DEFINO LOS COMPONENTES QUE VOY A USAR
let Login 		= LoginComponent
let Dashboard 	= DashboardComponent

// DEFINO UN ARRAY DE RUTAS
// CADA ITEM TIENE UN "path" ~ "component"
let routes = [
  { path: '/', component: Login },
  { path: '/dashboard', component: Dashboard },
]

// INSTANCIO EL RUTEO DE MANERA GLOBAL
let router = new VueRouter({
  routes
})

let app = new Vue({
  router,
  data: {
		mensaje: 'Hola Mundo!',
		currentRoute: window.location.pathname
	}
}).$mount('#app')