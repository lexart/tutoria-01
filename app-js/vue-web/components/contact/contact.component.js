Vue.component('contact-component', function (resolve, reject){
	// GENERO LA REQUEST PARA TRAERME EL VIEW POR EL PATH
	Servicio.view('./components/contact/contactView.html', function (view){
		// SI TODO ES OK 
		// RESUELVE EL OBJ ANTERIOR
		resolve({
			props:['title'],
			template: view,
			data: function (){
					return {
						itemsContact: [
							{
								titulo: "Formulario de contacto",
								nombre: '<label for="nombre">Nombres:  </label> <input type="text" name="" id="nombre">',
								apellido: '<label for="apellido">Apellidos:</label> <input type="text" name="" id="apellido">',
								email: '<input type="mail" name="" id="email">',
								asunto: '<input type="text" name="" id="asunto">',
								mensaje: '<input type="text" name="" id="mensaje">',
								btnText: 'Enviar'
							},
					
						],
						text: ''
				}
				
			},

		})
	})
})