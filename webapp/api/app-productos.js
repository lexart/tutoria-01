const express = require('express');
const cors 	  = require('cors');

const app 	  		= express();
const bodyParser 	= require('body-parser');
global.conn 	    = require('./config/conn');

const corsOptions 	= {
	  "origin": '*',
	  "methods": "GET,HEAD,PUT,POST,DELETE,OPTIONS",
	  "preflightContinue": false,
	  "allowedHeaders": ['Content-Type', 'Authorization'],
	  "optionsSuccessStatus": 204
}

app.use( cors(corsOptions) );
app.use( bodyParser.json() )

// 
// ABM Productos
// 
	// Obtener todos los productos
	app.get('/productos/all', async function (req, res) {
		let Producto 	= require('./servicios/productos')
		let response 	= await Producto.obtenerProductos()
	  
	  // SETEO COMO QUIERO SERVIR LA DATA
	  res.set(['Content-Type', 'application/json']);
	  res.send(response);
	});

	// Obtener producto por id
	app.get('/producto/:id', async function (req, res) {
		let id 			= req.params.id;
		let Producto 	= require('./servicios/productos')
		let response 	= await Producto.obtenerProductoPorId(id)
	  
	  // SETEO COMO QUIERO SERVIR LA DATA
	  res.set(['Content-Type', 'application/json']);
	  res.send(response);
	});

	// Crear un producto nuevo
	app.post('/producto/new', async function (req, res) {
		let post 		= req.body;
		let Producto 	= require('./servicios/productos')
		let response 	= await Producto.crearProducto(post)
	  
	  // SETEO COMO QUIERO SERVIR LA DATA
	  res.set(['Content-Type', 'application/json']);
	  res.send(response);
	});

	app.put('/producto/:id', async function (req, res) {
		// Capturo la respuesta del post
		let id   = req.params.id
		let post = req.body;

		let Producto 	= require('./servicios/productos')
		let response 	= await Producto.editarProducto(id, post)

		// SETEO COMO QUIERO SERVIR LA DATA
		res.set(['Content-Type', 'application/json']);
		res.send(response);
	});

	app.delete('/producto/delete/:id', async function (req, res) {
		let id   = req.params.id

		let Producto 	= require('./servicios/productos')
		let response 	= await Producto.eliminarProducto(id)

		// SETEO COMO QUIERO SERVIR LA DATA
		res.set(['Content-Type', 'application/json']);
		res.send(response);
	})

app.listen(3000, function () {
  console.log('Backend ABM: 3000');
});