const express = require('express');
const cors 	  = require('cors');

global.app 	  		= express();
const bodyParser 	= require('body-parser');
global.conn 	    = require('./config/conn');
global.env 	    	= require('./env');
const mid 			= require('./servicios/middleware')

// Jwt require
global.jwt = require('jsonwebtoken');

app.set('secret', env.secret);

const corsOptions 	= {
	  "origin": '*',
	  "methods": "GET,HEAD,PUT,POST,DELETE,OPTIONS",
	  "preflightContinue": false,
	  "allowedHeaders": ['Content-Type', 'Authorization', 'token'],
	  "optionsSuccessStatus": 204
}

app.use( cors(corsOptions) );
app.use( bodyParser.json() )
// app.use( mid.rutasProtegidas );

// 
// ABM Clientes
// 
	// Obtener todos los productos
	app.get('/clientes/all', mid.rutasProtegidas,  async (req, res) => {
		let Cliente 	= require('./servicios/clientes')
		let response 	= await Cliente.obtenerClientes()
	  
	  // SETEO COMO QUIERO SERVIR LA DATA
	  res.set(['Content-Type', 'application/json']);
	  res.send(response);
	});

	// Login de aplicación
	app.post('/login', async (req, res) => {
		let post 		= req.body;
		let Usuario 	= require('./servicios/usuarios')
		let Cliente 	= require('./servicios/clientes')
		let resUsuario 	= await Usuario.loginUsuario(post.usuario, post.clave)
		let usuario 	= resUsuario.response;
		let idCliente 	= usuario.cliente;

		usuario.cliente = await Cliente.obtenerClientePorId(idCliente);
		usuario.cliente = usuario.cliente.response;

		let response 	= {
			response: usuario
		}
	  
	  // SETEO COMO QUIERO SERVIR LA DATA
	  res.set(['Content-Type', 'application/json']);
	  res.send(response);
	});


app.listen(3000, function () {
  console.log('Backend ABM: 3000');
});