let Middleware = {
  rutasProtegidas: (req, res, next) => {
    const token = req.headers['token'];
    const host  = req.headers['host'];

    const rutas = {
        "roll9": ['/login','/clientes/all'],
        "roll1": ['/login']
    };


    console.log("req: ", req.url)
    // if(host.includes('3000')){
    //   return res.json({ error: 'No se aceptan request de puerto: 3000' });
    // }
   
     console.log("req.headers: ", req.headers)

      if (token) {
        jwt.verify(token, app.get('secret'), (err, decoded) => { 

          if (err) {
            return res.json({ error: 'Token invalido' });    
          } else {
            req.decoded = decoded;

            console.log("decoded: ", decoded)
            let idUsuario = decoded.idUsuario;

            if(rutas["roll" + idUsuario].includes(req.url)){
              next();
            } else {
              return res.json({ error: 'Usuario no permitido' });    
            }
            
          }
        });
      } else {
        res.send({ 
            error: 'Token vacio' 
        });
      }
  }
}

module.exports = Middleware;