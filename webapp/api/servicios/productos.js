let Producto = {
	obtenerProductos: async function (){
		let sql = `
			SELECT * FROM productos
		`
		// Lo seteo como un array vacio
		let productos = []
		let response = {
			error: "No existe productos"
		}

		productos = await conn.query(sql)

		if(productos.length > 0){
			response = {
				response: productos
			}
		}
		return response;
	},
	obtenerProductoPorId: async function (id){
		let sql = `
			SELECT * FROM productos WHERE id = ${id}
		`
		// Lo seteo como un array vacio
		let productos = []
		let response = {
			error: "No existe el producto"
		}

		productos = await conn.query(sql)

		if(productos.length > 0){
			response = {
				response: productos[0]
			}
		}
		return response;
	},
	crearProducto: async function (prod){
		let sql  = `
		  	INSERT INTO productos
		  		(
		  			nombre
		  		)
		  	VALUES
		  		(
		  			'${prod.nombre}'
		  	)
		  `
	  
	  let response = {
	  	error: `Error al ingresar el producto.`
	  }

	  let producto = []
	  producto = await conn.query(sql)

	  if(producto.insertId){
	  	response = {
	  		response: 'Producto ingresado correctamente.'
	  	}
	  }
	  return response;
	},
	editarProducto: async function (id, prod){
		let sql  = `
		  	UPDATE productos
		  		SET nombre = '${prod.nombre}'
		  	WHERE id = ${id}
		  `
	  
	  let response = {
	  	error: `Error al editar el producto.`
	  }

	  let producto = []
	  producto = await conn.query(sql)

	  if(producto.changedRows > 0){
	  	response = {
	  		response: 'Producto editado correctamente.'
	  	}
	  }
	  return response;
	},
	eliminarProducto: async function (id){
	  let sql  = `
	  	DELETE FROM productos WHERE id = ${id}
	  `
	  
	  let response = {
	  	error: `Error al eliminar el producto.`
	  }

	  let producto = []
	  producto = await conn.query(sql)

	  if(producto.affectedRows > 0){
	  	// Por que es un usuario
	  	response = {
	  		response: 'Producto eliminado correctamente.'
	  	}
	  }
	  return response;
	}
}

module.exports = Producto;