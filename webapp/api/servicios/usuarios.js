let Usuario = {
	obtenerUsuarios: async function (){
		let sql = `
		  	SELECT * FROM usuarios
		  `
		  // Lo seteo como un array vacio
		  let usuarios = []
		  let response = {
		  	error: "No existe usuarios"
		  }

		  usuarios = await conn.query(sql)

		  if(usuarios.length > 0){
		  	response = {
		  		response: usuarios
		  	}
		  }
		return response;
	},
	obtenerUsuarioPorId: async function (id){
	  let sql = `
	  	SELECT * FROM usuarios WHERE id = ${id}
	  `
	  // Lo seteo como un array vacio
	  let usuarios = []
	  let response = {
	  	error: `No existe el usuario con ID: ${id}`
	  }

	  usuarios = await conn.query(sql)

	  if(usuarios.length > 0){
	  	// Por que es un usuario
	  	response = {
	  		response: usuarios[0]
	  	}
	  }
	  return response;
	},
	loginUsuario: async function (usuario, clave){
		let sql = `
			SELECT idCliente AS cliente, nombre, usuario, id FROM usuarios
			WHERE usuario = '${usuario}' AND clave = MD5('${clave}')
		`
		let usuarios = []
		let response = {
			error: `No existe el usuario: ${usuario}`
		}

		usuarios = await conn.query(sql)

		if(usuarios.length > 0){
			// Por que es un usuario
			let usuario 	= usuarios[0];
			let mockHash 	= {idUsuario: usuario.id};

			console.log("usuario.id: ", usuario.id)
			// Genero token con JWT
			const token = jwt.sign(mockHash, app.get('secret'), {
			   expiresIn: 1440
			});

			usuario.token = token;

			response = {
				response: usuario
			}
		}
		return response;
	}
}
module.exports = Usuario;