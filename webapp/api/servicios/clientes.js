let Cliente = {
	obtenerClientes: async function (){
		let sql = `
			SELECT * FROM clientes
		`
		// Lo seteo como un array vacio
		let clientes = []
		let response = {
			error: "No existe clientes"
		}

		clientes = await conn.query(sql)

		// Verificar el tipo de cliente
		let tipos = [
			{id: 1, nombre: "Empresa"},
			{id: 2, nombre: "Consumidor final"}
		]

		if(clientes.length > 0){

			// Mapeo los clientes que me trae de la base
			clientes.map( (item) => {
				// Mapeo los tipos de cliente
				tipos.map( (tipo) => {
					// Verifico si el tipo que viene de la base es igual a la
					// id del tipo de cliente
					if(item.tipo == tipo.id){
						item.tipo = tipo;
					}
				})
			})

			response = {
				response: clientes
			}
		}
		return response;
	},
	obtenerClientePorId: async function (id){
		let sql = `
			SELECT * FROM clientes WHERE id = ${id}
		`
		// Lo seteo como un array vacio
		let clientes = []
		let response = {
			error: "No existe el cliente"
		}

		clientes = await conn.query(sql)

		// Verificar el tipo de cliente
		let tipos = [
			{id: 1, nombre: "Empresa"},
			{id: 2, nombre: "Consumidor final"}
		]

		if(clientes.length > 0){
			let cliente = clientes[0];

			tipos.map( (tipo) => {
				// Verifico si el tipo que viene de la base es igual a la
				// id del tipo de cliente
				if(cliente.tipo == tipo.id){
					cliente.tipo = tipo;
				}
			})



			response = {
				response: cliente
			}
			console.log("response: ", response)
		}
		return response;
	},
	crearProducto: async function (prod){
		let sql  = `
		  	INSERT INTO productos
		  		(
		  			nombre
		  		)
		  	VALUES
		  		(
		  			'${prod.nombre}'
		  	)
		  `
	  
	  let response = {
	  	error: `Error al ingresar el producto.`
	  }

	  let producto = []
	  producto = await conn.query(sql)

	  if(producto.insertId){
	  	response = {
	  		response: 'Producto ingresado correctamente.'
	  	}
	  }
	  return response;
	},
	editarProducto: async function (id, prod){
		let sql  = `
		  	UPDATE productos
		  		SET nombre = '${prod.nombre}'
		  	WHERE id = ${id}
		  `
	  
	  let response = {
	  	error: `Error al editar el producto.`
	  }

	  let producto = []
	  producto = await conn.query(sql)

	  if(producto.changedRows > 0){
	  	response = {
	  		response: 'Producto editado correctamente.'
	  	}
	  }
	  return response;
	},
	eliminarProducto: async function (id){
	  let sql  = `
	  	DELETE FROM productos WHERE id = ${id}
	  `
	  
	  let response = {
	  	error: `Error al eliminar el producto.`
	  }

	  let producto = []
	  producto = await conn.query(sql)

	  if(producto.affectedRows > 0){
	  	// Por que es un usuario
	  	response = {
	  		response: 'Producto eliminado correctamente.'
	  	}
	  }
	  return response;
	}
}

module.exports = Cliente;