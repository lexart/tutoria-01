let Cliente = {
	obtenerClientes: async function (){
		// Query: SELECT * FROM clientes
		let sql = `
			SELECT * FROM clientes
		`

		let res = {
			error: "No existen clientes"
		}

		let clientes = await conn.query(sql);

		// clientes = [] ~ simular error
		
		if(clientes.length > 0){
			res = {
				response: clientes
			}
		}

		return res;
	},
	obtenerClientePorId: async function (idCliente){
		// Query: SELECT * FROM clientes
		let sql = `
			SELECT * FROM clientes WHERE id = ${idCliente}
		`

		let res = {
			error: "No existe cliente"
		}

		let clientes = await conn.query(sql);

		// clientes = [] ~ simular error
		
		if(clientes.length > 0){
			res = {
				response: clientes[0]
			}
		}

		return res;
	},
	insertCliente: async function (cliente){
		let sql = `
			INSERT INTO clientes 
			(nombre, tipo)
			VALUES
			('${cliente.nombre}', '${cliente.tipo}')
		`

		let res = {
			error: "Error al insertar cliente"
		}

		let clientes = await conn.query(sql);

		// InsertId solo existe cuando registras en la base de datos
		if(clientes.insertId){
			res = {
				response: 'Cliente creado con éxito'
			}
		}

		return res;
	},
	editarCliente: async function (idCliente, cliente){
		let sql = `
			UPDATE clientes 
			SET
				nombre = '${cliente.nombre}',
				tipo   = '${cliente.tipo}'
			WHERE
				id = ${idCliente}
		`

		let res = {
			error: "Error al actualizar cliente"
		}

		let clientes = await conn.query(sql);

		// changedRows se va a fijar las filas que se modificaron
		if(clientes.changedRows > 0){
			res = {
				response: 'Cliente editado con éxito'
			}
		}

		return res;
	}
}
module.exports = Cliente;