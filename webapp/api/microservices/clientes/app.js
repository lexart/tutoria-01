const express = require('express');
const app = express();
const bodyParser 	= require('body-parser');
// Requerir archivo config/conn
global.conn 			= require('./config/conn');

app.use( bodyParser.json() )

// 
app.get('/clientes/all', async function (req, res) {
  const clientes = require('./services/clientes')

  let arr = await clientes.obtenerClientes()

  // SETEO COMO QUIERO SERVIR LA DATA
  res.set(['Content-Type', 'application/json']);
  res.send(arr);
});


app.get('/clientes/:idCliente', async function (req, res) {
  const clientes = require('./services/clientes')
  let idCliente = req.params.idCliente;

  let arr = await clientes.obtenerClientePorId(idCliente);

  // SETEO COMO QUIERO SERVIR LA DATA
  res.set(['Content-Type', 'application/json']);
  res.send(arr);
});

app.post('/clientes/new', async function (req, res){
  const clientes = require('./services/clientes')

  let post = req.body;

  let arr = await clientes.insertCliente(post);

  res.set(['Content-Type', 'application/json']);
  res.send(arr)
});

app.put('/clientes/edit/:idCliente', async function (req, res){
  const clientes = require('./services/clientes')

  let post       = req.body;
  let idCliente  = req.params.idCliente;

  let arr = await clientes.editarCliente(idCliente, post);

  res.set(['Content-Type', 'application/json']);
  res.send(arr)
});

app.post('/', function (req, res){
	let data = req.body;

	res.set(['Content-Type', 'application/json']);
	res.send(data)
});

app.listen(3000, function () {
  console.log('Backend ABM Clientes: 3000');
});