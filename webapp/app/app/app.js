// DEFINO LOS COMPONENTES QUE VOY A USAR
let Login 		= LoginComponent
let Dashboard = DashboardComponent
let Usuarios 	= UsuariosComponent
let Usuario 	= UsuarioComponent
let Productos = ProductosComponent
let Producto  = ProductoComponent

const API 		= "http://localhost:3000/"

// DEFINO UN ARRAY DE RUTAS
// CADA ITEM TIENE UN "path" ~ "component"
let routes = [
  { path: '/', component: Login },
  { path: '/dashboard', component: Dashboard },
  { path: '/usuarios', component: Usuarios },
  { path: '/usuario/:id', component: Usuario },
  { path: '/usuario/new', component: Usuario },
  { path: '/productos', component: Productos },
  { path: '/producto/new', component: Producto },
  { path: '/producto/:id', component: Producto }
]

// INSTANCIO EL RUTEO DE MANERA GLOBAL
let router = new VueRouter({
  routes
})

let app = new Vue({
  router,
  data: {
		mensaje: 'Hola Mundo!',
		currentRoute: window.location.pathname
	}
}).$mount('#app')