let DashboardComponent = Vue.component('dashboard-component', function (resolve){
	axios.get('./app/components/dashboard/dashboardView.html').then( function (res){
		let view = res.data;

		resolve({
			template: view,
			data: function (){
				return {
					titulo: "Bienvenido, "
				}
			},
			methods: {
				logOut: function (){
					router.push({path: '/'})
				}
			},
			// EL METODO MOUNTED se encarga de ejecutar luego de que el componente
			// esté disponible
			mounted: function (){
				let usuario = window.localStorage.getItem('usuario');

				if(usuario == null){
					router.push({path:'/'})
				}

				this.titulo += usuario;

				console.log("mounted user :", usuario)
			}
		})
	})
})