let UsuariosComponent = Vue.component('usuarios-component', function (resolve){
	axios.get('./app/components/usuarios/usuariosView.html').then( function (res){
		let view = res.data;

		resolve({
			template: view,
			data: function (){
				return {
					titulo: "Usuarios",
					usuarios: []
				}
			},
			methods: {
				logOut: function (){
					router.push({path: '/'})
				}
			},
			// EL METODO MOUNTED se encarga de ejecutar luego de que el componente
			// esté disponible
			mounted: function (){
				let usuario = window.localStorage.getItem('usuario');

				if(usuario == null){
					router.push({path:'/'})
				}

				axios.get(API + 'usuarios/all').then( (resp) => {
					// Verifico que la API no me devuelva error
					if(!resp.data.error){
						// Agrego el array de usuarios dentro del 
						// array "this.usuarios"
						this.usuarios = resp.data.response
					} else {
						console.log("Error no carga usuarios")
					}
				})

				console.log("mounted usuarios :", API)
			}
		})
	})
})