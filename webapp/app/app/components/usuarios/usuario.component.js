let UsuarioComponent = Vue.component('usuario-component', function (resolve){
	axios.get('./app/components/usuarios/usuarioFormView.html').then( function (res){
		let view = res.data;

		resolve({
			template: view,
			data: function (){
				return {
					titulo: "Usuario",
					usuario: {
						"id": 0,
			            "nombre": "",
			            "apellido": "",
			            "usuario": "",
			            "clave": "",
			            "activo": 1
					}
				}
			},
			methods: {
				logOut: function (){
					router.push({path: '/'})
				},
				guardarUsuario: function () {
					console.log("usuario a guardar: ", this.usuario)
					if(this.usuario.id !== 0){
						axios.put(API + 'usuario/edit/' + this.usuario.id, 
							this.usuario).then( (resp) => {
								// Si no da erro nuestra
								if(!resp.data.error){
									router.push({path:'/usuarios'})
								} else {
									alert(resp.data.error)
								}
						})
					} else {
						// Crear el usuario
						axios.post(API + 'usuario/new', 
							this.usuario).then( (resp) => {
								// Si no da erro nuestra
								if(!resp.data.error){
									router.push({path:'/usuarios'})
								} else {
									alert(resp.data.error)
								}
						})
					}
				},
				eliminarUsuario: function (){
					if(confirm("Esta seguro que quiere eliminar el usuario?")){
						axios.delete(API + 'usuario/delete/' + this.usuario.id, 
							{}).then( (resp) => {
								// Si no da erro nuestra
								if(!resp.data.error){
									router.push({path:'/usuarios'})
								} else {
									alert(resp.data.error)
								}
						})
					}
				}
			},
			// EL METODO MOUNTED se encarga de ejecutar luego de que el componente
			// esté disponible
			mounted: function (){
				let usuario = window.localStorage.getItem('usuario');
				let id      = router.app._route.params.id ? router.app._route.params.id : undefined

				// Si quiero editar
				if(id){
					console.log("quiero editar - usuario")
					axios.get(API + 'usuario/' + id).then( (resp) => {
						console.log("usuario: ", resp);
						// Obtener el usuario que me encapsula la API en el response
						this.usuario = resp.data.response;
					})
				// Si quiero crear
				} else {
					console.log("quiero crear - usuario")
				}

				if(usuario == null){
					router.push({path:'/'})
				}
			}
		})
	})
})