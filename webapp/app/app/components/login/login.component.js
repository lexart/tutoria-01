let LoginComponent = Vue.component('login-component', function (resolve, reject){
	// LLAMAR SERVICIO DE AXIOS
	// GET
	axios.get('./app/components/login/loginView.html').then( function (res){
		console.log("res: ", res)
		// DEVUELVE UN RESPONSE: res
		let view = res.data; // SACAMOS EL HTML DEL res.data

		resolve({
			// asignamos el HTML de la view al template
			template: view,
			// data sería el modelo del componente
			data: function (){
				return {
					titulo: "Login",
					objLogin: {
						usuario:'',
						clave:''
					}
				}
			},
			// methods en este caso sería nuestro controller
			methods: {
				loginUser: function (){
					let obj = this.objLogin;
					console.log("obj ::", obj);

					if(obj.usuario == 'alex' && obj.clave == '123'){
						// TRANSPORTAR USUARIO DE UN LADO A OTRO
						// GUARDO EN MEMORIA
						window.localStorage.setItem('usuario', obj.usuario);

						// COMO VOY AL DASHBOARD?
						console.log("success ::", router);

						// VAMOS DE / ~ /dashboard
						router.push({path: '/dashboard'});

					} else {
						console.log("error ::")
					}
				}
			},
			mounted: function (){
				window.localStorage.clear();
			}
		})
	})
})