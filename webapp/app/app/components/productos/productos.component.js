let ProductosComponent = Vue.component('productos-component', function (resolve){
	axios.get('./app/components/productos/productosView.html').then( function (res){
		let view = res.data;

		resolve({
			template: view,
			data: function (){
				return {
					titulo: "Productos",
					productos: []
				}
			},
			methods: {

			},
			// EL METODO MOUNTED se encarga de ejecutar luego de que el componente
			// esté disponible
			mounted: function (){
				// Llamada a la API 
				axios.get(API + 'productos/all').then( (res) => {
					console.log("Data desde la API: ", res);
					// Si no hay error
					if(!res.data.error){
						this.productos = res.data.response;
					}
				})
			}
		})
	})
})