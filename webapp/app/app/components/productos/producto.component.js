let ProductoComponent = Vue.component('producto-component', function (resolve){
	axios.get('./app/components/productos/productoView.html').then( function (res){
		let view = res.data;

		resolve({
			template: view,
			data: function (){
				return {
					titulo: "Form :: Producto",
					producto: {}
				}
			},
			methods: {
				guardarProducto: function (){
					console.log("producto ::", this.producto)
					// Si no tiene ID crean el producto
					if(!this.producto.id){
						axios.post(API + 'producto/new', this.producto).then( (res) => {
							console.log("Resultado de la API: ", res.data)
							if(!res.data.error){
								router.push({path: '/productos'})
							} else {
								alert(res.data.error)
							}
						})
					} else {
						axios.put(API + 'producto/' + this.producto.id, this.producto).then( (res) => {
							console.log("Resultado de la API: ", res.data)
							if(!res.data.error){
								router.push({path: '/productos'})
							} else {
								alert(res.data.error)
							}
						})
					}
				},
				eliminarProducto: function (){
					if(confirm("Esta seguro que desea eliminar el producto?")){
						axios.delete(API + 'producto/delete/' + this.producto.id).then( (res) => {
							console.log("Resultado de la API: ", res.data)
							if(!res.data.error){
								router.push({path: '/productos'})
							} else {
								alert(res.data.error)
							}
						})
					}
				}
			},
			// EL METODO MOUNTED se encarga de ejecutar luego de que el componente
			// esté disponible
			mounted: function (){
				let id = router.app._route.params.id ? router.app._route.params.id : undefined

				if(id){
					axios.get(API + 'producto/' + id).then( (res) => {
						console.log("Obtengo usuario por ID de la API: ", res.data)
						if(!res.data.error){
							this.producto = res.data.response;
						}
					})
				}
			}
		})
	})
})