let ClientesComponent = Vue.component('clientes-component', function (resolve){
	axios.get('./app/components/clientes/clientesView.html').then( function (res){
		let view = res.data;

		resolve({
			template: view,
			data: function (){
				return {
					titulo: "Clientes",
					nombre: "Alex",
					clientes: []
				}
			},
			methods: {

			},
			// EL METODO MOUNTED se encarga de ejecutar luego de que el componente
			// esté disponible
			mounted: function (){
				// Llamada a la API 
				console.log("clientes ::")

				axios.get(API + 'clientes/all').then( (res) => {
					let clientes = res.data;

					this.clientes = clientes;
				})
			}
		})
	})
})