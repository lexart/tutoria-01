// DEFINO LOS COMPONENTES QUE VOY A USAR
let Clientes  = ClientesComponent

const API 		= "http://localhost:3000/"

// DEFINO UN ARRAY DE RUTAS
// CADA ITEM TIENE UN "path" ~ "component"
let routes = [
  { path: '/', component: Clientes }
]

// INSTANCIO EL RUTEO DE MANERA GLOBAL
let router = new VueRouter({
  routes
})

let app = new Vue({
  router,
  data: {
		mensaje: 'Hola Mundo!',
		currentRoute: window.location.pathname
	}
}).$mount('#app')