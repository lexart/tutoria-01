- API: 
	Flow:
		ABM Clientes
		Auth: Login
			- Middleware
				- Rutas protegidas JWT

		ABM Clientes ~ API: /clientes/{urls}
			Ambiente: [microservices/clientes]
				Carpetas:
					services/
					config/
					app.js
					package.json
		
		ABM Flujo:

			- Listado de clientes
			- Obtener cliente por ID
			- Alta/Editar cliente
			- Baja cliente

		Autenticación:
			- Login

		Proteger las rutas dado un usuario
			- Hacemos con un middleware (JWT)

		Documentar en POSTMAN todo


		[Paso 0]
			npm init
			.gitignore

		[Paso 1]
			npm i --save express body-parser mysql cors jsonwebtoken

				- express: APP express + ruteo
				- body-parser: objeto body dentro del req que nos llega del postman: req.body
				- mysql: mysql driver para nodejs
				- cors: facilitador HTTP entre API y APP
				- jsonwebtoken: generador de token web para autenticación
			
			(Obs): 
				npm i -g nodemon (el parametro -g es global)
				El parametro "--save" es para guardar las dependencias en el package.json
				--> Si tenes una API de terceros para correr:
					> npm i
					> nodemon app.js

			(Obs2): para saber que paquetes faltan: npm list
		
		[Paso 2]
			Agregar carpeta config dentro de la API
				- dentro de la carpeta config vamos a tener los archivos de configuración con el mysql
			Agregar carpeta services/

		[Paso 3]
			- Copiar setup inicial y correr API de prueba
			- Corren: nodemon app.js
				- Debería quedar escuchando el puerto 3000

		[Paso 4]

			Listado de clientes:
				- Tabla clientes 
					Modelo: (id, nombre, rut, direccion, tel, activo)
				- Crear el ruteo para el listado de clientes:
					[Ruteo]: Básicamente es un URL donde HTTP va a interactuar con algún verbo
						
						Puedo hacer:
						GET de una URL (/clientes/all)
							
							- Obtiene información de esa URL (Ruteo)
							- De que forma? 
								Nos llega un JSON

						Puedo hacer un POST de una URL
						...

					- Ruteo: /clientes/all
						¿Cómo se define /clientes/all dentro de nuestra API?
							1) Dentro del app.js tenemos requerido el paquete de express
								- Cómo requerimos un paquete dentro del app.js? 
								- Definimos una constante igualado al paquete, de que manera?

									const express = require('express') 

									- De donde sale: "require('express')" ?
									* Sale de la url: node_modules/express/index.js

									Pero nodejs ya sabe que los paquetes de dependencia están dentro del "node_modules" entonces no tenemos por escribir toda la ruta dentro del "require"

							2) Tengo que invocar el express para crear el "ambiente" de API REST de nuestro app.js
								- "Ah, esto es una API Rest" (Entregar información orientado a estados)
								- Cómo lo hacemos?
									Invocamos express, de que manera?
									
									const app = express();

							3) Definir la ruta: /clientes/all

								app.get('/clientes/all', async function (req, res){...})

							 Desde acá sabemos que van a llegar peticiones de tipo GET desde POSTMAN (o navegador) y se va concentrar en la funcionalidad de este ruteo (es lo que devuelve)

								Como definimos la funcionalidad de "/clientes/all"?

									- ¿Que quiere decir "funcionalidad de clientes/all"?

										* Es lo que te devuelve como respuesta después de la petición GET /clientes/all
											- Puede devolver: JSON de clientes 
											- String
											- Numero
											- Papas
											...

									- Tenemos una función de este tipo: 
										> async function(req, res){...} ¿Donde está, ésta función?

											- Está en seguida después del ruteo:
											app.get('/clientes/all', async function (req, res){...})

										> Que hace? 
											- Se encarga de servir la información a través de la ruta

										> Que es el parametro: "req"?
											Req: ~ "Request" lo que me llega (datos) del cliente (Front-End: POSTMAN, Chrome, etc)
												
												¿req que información tiene?
													- Parametros de url
													- Headers de tipo request headers

										> Que es el parametro: "res"?
											Res: ~ "Response" lo que envío (datos desde el backend) al cliente (Front-End: POSTMAN, Chrome, etc)

												¿res que información tiene?
													- JSON (o lo que sea typo documento)
													- Headers de tipo response headers

										> Que hace el "async" antes del "function"?

											Les dice que: es un función donde existe una resolución de un promise (petición de datos con respecto al tiempo indeterminada) de forma lineal

												> Forma lineal
													let prom = await algo()

												> Forma funel (En cualquier function normal)
													let prom = {}
													
													algo().then( function(res){
														prom = res.data
													})

							4) ¿Cual es la salida de /clientes/all?

								- Sería una respuesta de tipo JSON, como la defino?
									- Dentro del function(req, res){...}, le voy a decir al "res" que tipo de salida quiero
									- Al "res" seteo el tipo de salida de la siguiente manera:
										
										res.set(['Content-Type', 'application/json']);

										- Content-Type: tipo de documento para la salida
										- application/json: es el tipo: JSON

									- Como envío la información hacia el front-end?
										Dentro del "res" existe un método llamado "send" donde transmite la información hacia el front-end de a siguiente manera: 

											res.send(arr);

										- Donde "arr" que es?
											La variable de tipo JavaScript


				- Crear servicio para clientes
					
					- Que es un "servicio"?

						Es un módulo que concentra la funcionalidad de una abstracción correspondiente. En este caso "clientes" y toda su lógica.

						- ¿Cómo lo defino?

							Defino un objeto con todas sus propiedades y métodos luego lo exporto. De que manera? 

							- module.exports = Cliente;

							Esto permite que pueda requerir ese modulo dentro de nuestra API Rest (clientes/all)

								- De que manera?

								Dentro del app.js, bajo la ruta clientes/all requerimos el modulo de clientes:

									const clientes = require('./services/clientes')

								Ahora el módulo de clientes está disponible dentro de nuestro ruteo con sus funcionalidades

								Una manera sencilla de probar el vínculo, es creando una función de prueba que retorne un objeto estático. En este caso creamos la función:

									obtenerClientes: function (){
										return {
											nombre: "Alex",
											apellido: "Casadevall"
										}
									}

									* Dentro del objeto Cliente ~> services/clientes.js (servicio)

								Luego bajo la ruta clientes/all invoco la función de prueba para ver el vínculo <servicio, ruteo> correctamente

									[Vínculo]: let arr = clientes.obtenerClientes()

						- Conectar mi servicio con la base de datos

							- ¿Cómo lo hago?

								- Requerir el módulo de MYSQL, dentro del config/conn.js
								- En el app.js requerir el módulo "Conn":

									const conn 	= require('./config/conn');

									* Se va a utilizar: conn.query(...) dentro del servicio clientes

							- ¿Cómo genero un listado de clientes dentro de mi servicio clientes.js?

								1) Definir cual es mi query?
									SELECT * FROM clientes
							    2) Agrego esa Query como un string dentro de la variable "sql", que está dentro de clientes.js

							    3) ¿Cómo comunico esta query con la función: conn.query?

							    	- let clientes = conn.query(sql); pero conn.query es una promise. Entonces obtenerClientes debe ser una async function y luego utilizamos el "await" para esperar los datos de la base

							    4) Probar el vínculo <mysql, servicio>: Sería que la base de datos está respondiendo al servicio

							    	[Vínculo]: let clientes = await conn.query(sql);

							    	(Obs): Para verificar el vínculo hago un console.log(clientes)

							    5) Exponer los datos que me trae el mysql en la ruta de clientes/all que seria el siguiente vínculo: 
							    	< <mysql, servicio> , ruteo >

							    	Dentro de obtenerClientes hago el return del vínculo de <mysql, servicio>

							    	[Vínculo]: return clientes;

							    6) El vínculo <servicio, ruteo> le tengo que agregar un await al let arr = await clientes.obtenerClientes()

							    	- Exponer la información cuando se hace un GET a clientes/all

							    	la variable "conn" para que tenga alcance en todos los ruteos la definimos global: "global.conn"


					- Probar: /clientes/all en POSTMAN

				- Comunicaciones del servicio

					¿Como se comunica un servicio?

						- Se comunica a través de un JSON

						¿Que estados de comunicación tiene?

							[Success]:
								Obtengo un objeto para el estado success cuando la información interpretada es válida

								{
									"response": clientes
								}

							[Error]:

								Obtengo un objeto para el estado error cuando la información interpretada es inválida

								{
									"error": "mensaje"
								}

								Interpretaciones de los errores:

									- Errores de infraestructura (500, 400)
									
									[ Dentro del sistema ]
										- Errores lógicos
											- Con respecto a la interpretación de la data
												- Validaciones de campo
												- La base no despliega datos


			[Obtener cliente por ID]: 

					[Base de datos]
						- Tabla de clientes:
						(id, nombre, rut, direccion, tel, activo)

					[API]
						GET /clientes/:idCliente ~  /clientes/1

					[Servicio]
						obtenerClientePorId(idCliente)
						~> Query MYSQL: 
							SELECT * FROM clientes WHERE id = ${idCliente}

					[Delivery de la data]

						~> Primero obtener el idCliente desde el parametro: req.params.idCliente
							* Si el ruteo es así: /clientes/:idCliente
							
							Entonces existe una propiedad dentro del objeto req.params que tiene el mismo nombre de :idCliente

							let idCliente = req.params.idCliente

								Ejemplo: /clientes/:id

								let idCliente = req.params.id

						~> let arr = await clientes.obtenerClientePorId(idCliente)

					[Output]
						res.send(arr)


			[Alta/Editar cliente]

				[ALTA]

					[Base de datos]
						- Tabla de clientes:
						(id, nombre, rut, direccion, tel, activo)

					[API]

						POST ({nombre, ...}) /clientes/new

					[Servicio]
						insertarCliente(cliente) 
							* Viene cliente como parametro de entrada

						Query: 
							* Dentro del template
								INSERT INTO clientes 
								(nombre, ...)
								VALUES
								('${cliente.nombre}', '${cliente.tipo}', ...)

					[Delivery de la data]

						POST /clientes/new (req, res)

						let post = req.body (Payload que llega del POSTMAN)
						
						[Ruteo] <-> [Servicio]
						
							let arr = await clientes.insertarCliente(post)

					[Output]
						res.send(arr)

				[EDITAR]

					[Base de datos]
						- Tabla de clientes:
						(id, nombre, rut, direccion, tel, activo)

					[API]
						PUT ({nombre, ...}) /clientes/edit/:idCliente

						let idCliente = req.params.idCliente
						let post 	  = req.body;

					[Servicio]
						editarCliente(idCliente, cliente) 
							* Viene cliente como parametro de entrada
							* Viene idCliente como parametro de URL

						Query: 
							* Dentro del template
								UPDATE clientes 
								SET 
									nombre = '${cliente.nombre}',
									tipo   = '${cliente.tipo}'
								WHERE 
									id = '${idCliente}'

					[Delivery de la data]
						PUT /clientes/edit/:idCliente (req, res)

						let post 		= req.body (Payload que llega del POSTMAN)
						let idCliente	= req.paramas.idCliente
						
						[Ruteo] <-> [Servicio]
						
							let arr = await clientes.editarCliente(idCliente, post)

					[Output]
						res.send(arr)








