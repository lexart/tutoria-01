const express = require('express');
const app = express();
const bodyParser 	= require('body-parser');

app.use( bodyParser.json() )

app.get('/', function (req, res) {
  let arr = [
  		{
  			nombre:"Manzana"
  		},
  		{
  			nombre:"Pera"
  		},
  		{
  			nombre:"Naranja"
  		}
  ];

  console.log("arr: ", arr);

  // SETEO COMO QUIERO SERVIR LA DATA
  res.set(['Content-Type', 'application/json']);
  res.send(arr);
});

app.post('/', function (req, res){
	let data = req.body;
	// COMUNICACION
	let response = {
		response: {
			mensaje: "Ingresado correctamente",
			postBody: data
		}
	}

	let error 	= {
		error: "Error al ingresar"
	}

	res.set(['Content-Type', 'application/json']);
	res.send(response)
});

app.listen(3000, function () {
  console.log('Escucho al puerto: 3000');
});