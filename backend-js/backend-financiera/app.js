const express = require('express');
const app = express();
const bodyParser 	= require('body-parser');

app.use( bodyParser.json() )

// 
app.get('/prestamo/:id', function (req, res) {

  // EL NOMBRE DEL PARAMETRO CON LA PROP. DEL OBJETO
  console.log("req params: ", req.params.id);

  // SETEO COMO QUIERO SERVIR LA DATA
  res.set(['Content-Type', 'application/json']);
  res.send({response: true});
});

app.post('/prestamos', function (req, res){
	let data = req.body;

  // LISTADO DE CLIENTES
  let clientes = [
    {
      "nombre":"Pepe",
      "edad":23,
      "sueldo":20000
    },
    {
      "nombre":"Julio",
      "edad":18,
      "sueldo":20000
    },
    {
      "nombre":"pedro",
      "edad":23,
      "sueldo":15000
    }
  ];

  // HABILITADOS PARA EL PRESTAMO
  // INICIALMENTE VACIO
  let resultado = [];

  for (let i = clientes.length - 1; i >= 0; i--) {
    let cliente = clientes[i];

    // SI CUMPLE LAS CONDICIONES DE LA BUSQUEDA
    if(cliente.edad >= data.edad && cliente.sueldo >= data.sueldo){
      // ENTOCES LO HABILITA
      resultado.push(cliente);
    }
  }

  console.log("data: ", data);

	res.set(['Content-Type', 'application/json']);
	res.send({response: resultado})
});

app.listen(3000, function () {
  console.log('Backend Financiera: 3000');
});