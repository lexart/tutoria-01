let Usuario = {
	obtenerUsuarios: async function (){
		let sql = `
		  	SELECT * FROM usuarios
		  `
		  // Lo seteo como un array vacio
		  let usuarios = []
		  let response = {
		  	error: "No existe usuarios"
		  }

		  usuarios = await conn.query(sql)

		  if(usuarios.length > 0){
		  	response = {
		  		response: usuarios
		  	}
		  }
		return response;
	},
	obtenerUsuarioPorId: async function (id){
	  let sql = `
	  	SELECT * FROM usuarios WHERE id = ${id}
	  `
	  // Lo seteo como un array vacio
	  let usuarios = []
	  let response = {
	  	error: `No existe el usuario con ID: ${id}`
	  }

	  usuarios = await conn.query(sql)

	  if(usuarios.length > 0){
	  	// Por que es un usuario
	  	response = {
	  		response: usuarios[0]
	  	}
	  }
	  return response;
	}
}
module.exports = Usuario;