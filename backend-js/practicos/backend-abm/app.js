const express = require('express');
const app = express();
const bodyParser 	= require('body-parser');
global.conn 	    = require('./config/conn');

app.use( bodyParser.json() )

// 
app.get('/', function (req, res) {
  let arr = "Hola Mundo"
  // SETEO COMO QUIERO SERVIR LA DATA
  res.set(['Content-Type', 'application/json']);
  res.send(arr);
});

app.post('/', function (req, res){
	let data = req.body;

	res.set(['Content-Type', 'application/json']);
	res.send(data)
});

// 
// ABM Usuarios
// 
	// Obtener todos los usuarios
	app.get('/usuarios/all', async function (req, res) {
		let Usuario 	= require('./servicios/usuarios')
		let response 	= await Usuario.obtenerUsuarios()
	  
	  // SETEO COMO QUIERO SERVIR LA DATA
	  res.set(['Content-Type', 'application/json']);
	  res.send(response);
	});

	// Obtener usuarios por ID
	app.get('/usuario/:id', async function (req, res) {
	  let id 		= req.params.id;
	  let Usuario 	= require('./servicios/usuarios')
	  let response 	= await Usuario.obtenerUsuarioPorId(id)

	  // SETEO COMO QUIERO SERVIR LA DATA
	  res.set(['Content-Type', 'application/json']);
	  res.send(response);
	});

	// Ingresar usuario nuevo
	app.post('/usuario/new', async function (req, res) {
	  // Capturo la respuesta del post
	  let post = req.body;
	  let sql  = `
	  	INSERT INTO usuarios
	  		(
	  			nombre,
	  			apellido,
	  			usuario,
	  			clave, 
	  			activo
	  		)
	  	VALUES
	  		(
	  			'${post.nombre}',
	  			'${post.apellido}',
	  			'${post.usuario}',
	  			MD5('${post.clave}'),
	  			1
	  	)
	  `
	  
	  let response = {
	  	error: `Error al ingresar el usuario.`
	  }

	  let usuario = []
	  usuario = await conn.query(sql)

	  if(usuario.insertId){
	  	// Por que es un usuario
	  	response = {
	  		response: 'Usuario ingresado correctamente.'
	  	}
	  }

	  // SETEO COMO QUIERO SERVIR LA DATA
	  res.set(['Content-Type', 'application/json']);
	  res.send(response);
	});

	app.put('/usuario/edit/:id', async function (req, res) {
	  // Capturo la respuesta del post
	  let id   = req.params.id
	  let post = req.body;
	  let sql  = `
	  	UPDATE usuarios SET
			nombre = '${post.nombre}',
			apellido = '${post.apellido}',
			usuario = '${post.usuario}',
			activo = ${post.activo}
		WHERE id = ${id}
	  `
	  
	  let response = {
	  	error: `Error al actualizar el usuario.`
	  }

	  let usuario = []
	  usuario = await conn.query(sql)

	  if(usuario.changedRows > 0){
	  	// Por que es un usuario
	  	response = {
	  		response: 'Usuario actualizado correctamente.'
	  	}
	  }

	  // SETEO COMO QUIERO SERVIR LA DATA
	  res.set(['Content-Type', 'application/json']);
	  res.send(response);
	});

	// 
	app.delete('/usuario/delete/:id', async function (req, res) {
	  // Capturo la respuesta del post
	  let id   = req.params.id
	  let post = req.body;
	  let sql  = `
	  	DELETE FROM usuarios WHERE id = ${id}
	  `
	  
	  let response = {
	  	error: `Error al eliminar el usuario.`
	  }

	  let usuario = []
	  usuario = await conn.query(sql)

	  console.log("usuario sql: ", usuario, sql)

	  if(usuario.affectedRows > 0){
	  	// Por que es un usuario
	  	response = {
	  		response: 'Usuario eliminado correctamente.'
	  	}
	  }

	  // SETEO COMO QUIERO SERVIR LA DATA
	  res.set(['Content-Type', 'application/json']);
	  res.send(response);
	});

app.listen(3000, function () {
  console.log('Backend ABM: 3000');
});