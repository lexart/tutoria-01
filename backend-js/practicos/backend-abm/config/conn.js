const mysql      	= require('mysql');
const connection 	= mysql.createConnection({
  host     : '127.0.0.1',
  user     : 'root',
  password : '',
  database : 'abm_sistema'
});
 
connection.connect();
 
let query = (sql) => {
	return new Promise( (resolve) => {
		connection.query(`${sql}`, function (error, results, fields) {
		  resolve(results);
		});
	})
}

let Conn = {
	query: query
}

module.exports = Conn