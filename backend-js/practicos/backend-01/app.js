const express 		= require('express');
const app 			= express();
const bodyParser 	= require('body-parser');

app.use( bodyParser.json() )

//Ruteo de la API

	// POST localhost:3000/
	app.post('/productos', function (req, res){
		let productos = [
			 {
				"response": {
					"nombre":"Alex",
					"apellido":"Casadevall"
					"documento":5100836,
					"edad":28,
				}
			}
		]

		// Objeto que llega del POSTMAN
		// {"cod": "11", "nombre":"hola"}
		let data = req.body;

		let resultados = []
		let response   = {
				error: {
					mensaje:"Producto no encontrado"
			}
		}

		// Busqueda del producto
		for (let i = productos.length - 1; i >= 0; i--) {
			let producto = productos[i]

			// Busqueda: 11
			if(producto.cod.includes(data.cod) || producto.nombre.includes(data.nombre)){
				resultados.push(producto)
			}
		}
		// Si encontró algo:
		if(resultados.length > 0){
			response   = {
				response: resultados
			}	
		}
		

		res.set(['Content-Type', 'application/json']);
		res.send(response)
	});

// Este es el puerto que escucha
// Url: localhost:3000
app.listen(3000, function () {
  console.log('Backend Financiera: 3000');
});