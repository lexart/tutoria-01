const express = require('express');
const app = express();
const bodyParser 	= require('body-parser');
// Requerir archivo config/conn
const conn 			= require('./config/conn');

app.use( bodyParser.json() )

// 
app.get('/', function (req, res) {
  let arr = "Hola Mundo"
  // SETEO COMO QUIERO SERVIR LA DATA
  res.set(['Content-Type', 'application/json']);
  res.send(arr);
});

app.post('/', function (req, res){
	let data = req.body;

	res.set(['Content-Type', 'application/json']);
	res.send(data)
});

app.listen(3000, function () {
  console.log('Backend Financiera: 3000');
});