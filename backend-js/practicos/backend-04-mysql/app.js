const express = require('express');
const app = express();
const bodyParser 	= require('body-parser');
const conn 			= require('./config/conn');

app.use( bodyParser.json() )

// Para esperar la respuesta de la base
app.get('/', async function (req, res) {
  let arr = "Hola mundo";
  let sql = `
  	SELECT probabilidad, valor, mensaje AS recomendacion FROM probabilidades
  `
  // Espera que se resuelva la query
  let resultados = await conn.query(sql)

  console.log("resultados: ", resultados)
  // SETEO COMO QUIERO SERVIR LA DATA
  res.set(['Content-Type', 'application/json']);
  res.send({response: resultados});
});

// Siempre que vaya a usar la conexión 
// Crear una funcion async
app.post('/', async function (req, res){
	let data = req.body;

	let sql = `
		SELECT probabilidad, valor, mensaje AS recomendacion FROM probabilidades
	`
	// Espera que se resuelva la query
	let resultados = await conn.query(sql)

	
	let probabilidades = {}

	// Convertir el array del select en un mapa
	resultados.map( (prob) => {
		probabilidades[prob.valor] = {
			probabilidad: prob.probabilidad,
			recomendacion: prob.recomendacion
		}
	})

	// Si 1 y figura
	// Entonces el resultado era 11 + figura: 10

	// Si  1 y numero
	// Entonces el resultado es la 1 + numero

	// Otro caso > 1 + numero o > 1 + figura
	let resultado = 0;
	let jugada 	  = data.cartas

	// Verifico si es figura o numero
	jugada.map( (carta, i) => {
		let isFigura =  parseInt(carta);

		// Estoy verificando si es un numero
		// ["J", 1]
		if(typeof carta == "number"){
			let jugadaAnt = (typeof jugada[i-1] == "string")

			if(carta == 1 && resultado == 10 && jugadaAnt){
				resultado += 11
			} else {
				resultado += isFigura
			}

		} else {
			// SI Jugada anterior es 1
			if(jugada[i-1] == 1){
				resultado = 21
			} else {
				resultado += 10
			}
		}
		// console.log("isFigura: ", isFigura)
	})

	let response = {
			"probabilidad":0,
			"recomendacion":"pedir"
	}

	try {
		response = probabilidades[resultado]
	} catch(e){}

	if(resultado > 21){
		response = {
			"error":"Error al procesar la probabilidad."
		}
	}

	res.set(['Content-Type', 'application/json']);
	res.send(response)
});

app.listen(3000, function () {
  console.log('Backend Financiera: 3000');
});