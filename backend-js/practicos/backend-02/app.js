const express = require('express');
const app = express();
const bodyParser 	= require('body-parser');

app.use( bodyParser.json() )

// 
app.get('/:id/:documento', function (req, res) {

  // Viaja por GET
  let id 		 = req.params.id
  let documento  = req.params.documento

  let personas   = [
  	{
  		"id":1,
  		"nombre":"Alex",
		"apellido":"Casadevall",
		"documento":5100836,
		"edad":28
  	},
  	{
  		"id":12,
  		"nombre":"Juan",
		"apellido":"Casadevall",
		"documento":51110123,
		"edad":25
  	},
  	{
  		"id":1111,
  		"nombre":"Gabriel",
		"apellido":"Casadevall",
		"documento":12312312,
		"edad":23
  	}
  ]

	let resultados = []
	let response   = {
		error: {
			"mensaje":"Usuario no encontrado"
		}
	}

  // Busqueda del producto
	for (let i = personas.length - 1; i >= 0; i--) {
		let persona = personas[i]

		// Busqueda: 11
		if(persona.id == id || persona.documento == documento){
			resultados.push(persona)
		}
	}
	// Si encontró algo:
	if(resultados.length > 0){
		response   = {
			response: resultados
		}	
	}


  // SETEO COMO QUIERO SERVIR LA DATA
  res.set(['Content-Type', 'application/json']);
  res.send(response);
});

app.listen(3000, function () {
  console.log('Backend Financiera: 3000');
});