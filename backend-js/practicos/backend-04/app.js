const express = require('express');
const app = express();
const bodyParser 	= require('body-parser');

app.use( bodyParser.json() )

app.post('/', function (req, res){
	let data = req.body;

	// Si 1 y figura
	// Entonces el resultado era 11 + figura: 10

	// Si  1 y numero
	// Entonces el resultado es la 1 + numero

	// Otro caso > 1 + numero o > 1 + figura
	let resultado = 0;
	let jugada 	  = data.cartas

	// Verifico si es figura o numero
	jugada.map( (carta, i) => {
		let isFigura =  parseInt(carta);

		// Estoy verificando si es un numero
		// ["J", 1]
		if(typeof carta == "number"){
			let jugadaAnt = (typeof jugada[i-1] == "string")

			if(carta == 1 && resultado == 10 && jugadaAnt){
				resultado += 11
			} else {
				resultado += isFigura
			}

		} else {
			// SI Jugada anterior es 1
			if(jugada[i-1] == 1){
				resultado = 21
			} else {
				resultado += 10
			}
		}
		// console.log("isFigura: ", isFigura)
	})

	let probabilidades = {
		"21": {
			"probabilidad":100,
			"recomendacion":"no pedir"
		},
		"20":{
			"probabilidad":92,
			"recomendacion":"no pedir"
		},
		"19":{
			"probabilidad":85,
			"recomendacion":"no pedir"
		},
		"18":{
			"probabilidad":77,
			"recomendacion":"no pedir"
		},
		"17":{
			"probabilidad":69,
			"recomendacion":"no pedir"
		},
		"16":{
			"probabilidad":62,
			"recomendacion":"no pedir"
		},
		"15":{
			"probabilidad":58,
			"recomendacion":"no pedir"
		},
		"14":{
			"probabilidad":56,
			"recomendacion":"no pedir"
		},
		"13":{
			"probabilidad":36,
			"recomendacion":"pedir"
		},
		"12":{
			"probabilidad":31,
			"recomendacion":"pedir"
		}
	}

	let response = {
			"probabilidad":0,
			"recomendacion":"pedir"
	}

	try {
		response = probabilidades[resultado]
	} catch(e){}

	if(resultado > 21){
		response = {
			"error":"Error al procesar la probabilidad."
		}
	}

	res.set(['Content-Type', 'application/json']);
	res.send(response)
});

app.listen(3000, function () {
  console.log('Backend Financiera: 3000');
});