// Logica de operaciones
let Operaciones = {
	suma: function (a, b){
		return a + b
	},
	resta: function (a, b){
		return a - b
	},
	multi: function (a, b){
		return a * b
	},
	div: function (a, b){
		let numerador = a;
		let divisor   = b;

		if(divisor != 0){
			return numerador / divisor
		} else {
			return {"error": "Error al dividir entre 0"}
		}
	}
}
module.exports = Operaciones;