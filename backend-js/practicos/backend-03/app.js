const express = require('express');
const app = express();
const bodyParser 	= require('body-parser');

app.use( bodyParser.json() )

// 
app.get('/', function (req, res) {
  let arr = "Hola"
  // SETEO COMO QUIERO SERVIR LA DATA
  res.set(['Content-Type', 'application/json']);
  res.send(arr);
});

app.post('/', function (req, res){
	let data = req.body;

	res.set(['Content-Type', 'application/json']);
	res.send(data)
});

app.post('/operaciones', function (req, res){
	// Llega el Post del POSTMAN
	let data = req.body;
	// Seteo resulato a 0
	let resultado = 0
	// Definio el response con resutado 0
	let response  = {
						error: {
							message:"Error de operación y/o cálculo."
						}
					}

	let operaciones = require('./services/operaciones');

	if(data.operacion){
		let tipo = data.operacion;
		let a 	 = data.a;
		let b 	 = data.b;
		let fallo 	= false;

		// Intento con la operación que me llega
		// Intento la instrucción en el caso critico
		try {
			resultado = operaciones[tipo](a, b)
		} catch (e){
			fallo = true;
		}
		// Si no fallo y no vino error de operaciones
		if(!fallo && !resultado.error){
			response = {resultado: resultado}
		}
	}

	res.set(['Content-Type', 'application/json']);
	res.send(response)
});

app.listen(3000, function () {
  console.log('Backend Financiera: 3000');
});