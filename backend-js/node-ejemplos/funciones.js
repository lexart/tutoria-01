// DEFINO SUMA
let suma = function (a, b){
	return a + b;
}
let resta = function (a, b){
	return a - b;
}

// EXPORTAR LA FUNCION SUMA EN UN MODULA
// ASI LO PUEDO REQUERIR EN OTRO ARCHIVO
module.exports = {
	suma: suma,
	resta: resta
}