// DEFINICION DE VARIABLES:
let num = 10
let obj = {
	nombre: "Alex",
	apellido: "Casadevall"
}
// SERIA COMO EL WINDOW
global.nombre = "Hola Mundo";

let suma = function (a, b){
	return a + b;
}
let arr  = [
	{
		nombre: "Manzana"
	},
	{
		nombre: "Pera"
	}
]

for (var i = arr.length - 1; i >= 0; i--) {
	console.log("producto: ", arr[i].nombre)
}

for (key in obj){
	console.log("key: ", key);
	console.log("obj: ", obj[key]);
}

let resultado = suma(num, 15);

console.log("resultado ::", resultado);

// FUNCION TEMPORAL
// setInterval( function (argument) {
// 	console.log("global: ", {nombre: nombre});
// }, 1000)
