const express = require('express');
const app = express();
const bodyParser 	= require('body-parser');

app.use( bodyParser.json() )

// 
app.get('/', function (req, res) {

  // SETEO COMO QUIERO SERVIR LA DATA
  res.set(['Content-Type', 'application/json']);
  res.send(arr);
});

app.post('/carrito/checkout', function (req, res){
	let productos = req.body;
	
	let resultado = {
		subtotal: 0,
		total: 0
	}

	for (let i = productos.length - 1; i >= 0; i--) {
		let producto = productos[i];

		resultado.subtotal += producto.cantidad * producto.precio;
	}

	resultado.total = resultado.subtotal * 1.22;

	res.set(['Content-Type', 'application/json']);
	res.send(resultado)
});

app.listen(3000, function () {
  console.log('Backend Tienda: 3000');
});