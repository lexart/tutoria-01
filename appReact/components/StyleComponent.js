let StyleComponent = {
    view: {
      flex: 1, 
      alignItems: 'center', 
      justifyContent: 'center'
    },
    textInput: { 
      width: 200, 
      height: 40, 
      borderColor: 'gray', 
      borderWidth: 1,
      marginBottom: 15
    },
    btn: {
      borderWidth: 1
    },
    tinyLogo: {
      width: 50,
      height: 50,
      marginBottom: 15
    }
}
module.exports = StyleComponent;