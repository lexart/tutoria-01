import * as React from 'react';
import { View, Text } from 'react-native';

function DetailsScreen({ route, navigation }) {
  const { usuarioNombre, usuarioId, usuarioRol, usuarioApellido } = route.params
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Text>Id: {usuarioId}</Text>
      <Text>Nombre: {usuarioNombre} {usuarioApellido}</Text>
      <Text>Rol: {usuarioRol}</Text>
    </View>
  );
}

export default DetailsScreen;