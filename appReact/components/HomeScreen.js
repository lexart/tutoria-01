import * as React from 'react';
import { View, Text, Button, TextInput, Image } from 'react-native';
import axios from 'axios';
import StyleComponent from './StyleComponent';

function HomeScreen({ navigation }) {
  
  console.log("StyleComponent: ", StyleComponent)

  const [email, setEmail] = React.useState('')
  const [clave, setClave] = React.useState('')
  const API = 'https://server134-school.lexartlabs.com/api-or-dev/';

  // Igualan la variable del modulo
  const Style = StyleComponent;

  const loginForm =  () => {
  	const formObj = {
  		usuarioEmail: email,
  		usuarioPassword: clave
  	}

    const headers = {
      header: {'Content-Type': 'application/json'}
    }

    axios.post(API + 'usuario/login', formObj, headers).then( (res) => {
      let response = res.data.response;

      console.log("response ::", response)
      navigation.navigate('Details', response)
    })

  	console.log("handlerForm - formObj ::", formObj)
  }

  // Return view
  return (
    <View style={Style.view}>
      <Image
        style={Style.tinyLogo}
        source={{
          uri: 'https://lexartlabs.com//logo-card.png',
        }}
      />
      <TextInput
	      style={Style.textInput}
	      value={email}
	      onChangeText={setEmail}
	    />
	  <TextInput
	  	  secureTextEntry={true}
	      style={Style.textInput}
	      value={clave}
	      onChangeText={setClave}
	  />
      <Button
      	style={Style.btn}
        title="Ingresar"
        onPress={() => loginForm()}
      />
    </View>
  );
}

export default HomeScreen;