const app = require('express')();
const http = require('http').createServer(app);
const io = require('socket.io')(http);

app.get('/', (req, res) => {
  res.send({hello: "World"});
});


io.on('connection', (socket) => {
  console.log('a user connected');

  // Usuario desconectado
  socket.on('disconnect', () => {
    console.log('user disconnected');
  });

  // Recibir mensajes
  socket.on('msg', (msg) => {
  	// Emite el mismo mensaje
  	console.log("sender :: ", msg)

  	// Quien emite el mensaje es el "io"
  	io.emit('msg', msg)
  })
});

http.listen(3000, () => {
  console.log('listening on *:3000');
});